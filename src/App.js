import React from "react"
import './App.css';
import Timezones from "./components/Timezones";

function App() {
  return (
    <div className="App">
      <Timezones/>
    </div>
  );
}

export default App;
