import {combineReducers} from "redux";
import timezonesReducer from "./timezones"

const rootReducer = combineReducers({
  timezonesState: timezonesReducer
})

export default rootReducer;
