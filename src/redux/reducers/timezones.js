import {TIMEZONES_LIST_SET} from "../const/timezones";

const initialState = {
  list: {}
}

export default function timezonesReducer(state = initialState, action){

  switch(action.type){
    case TIMEZONES_LIST_SET:
      return {
        list: action.data
      }

    default:
      return state
  }
}
