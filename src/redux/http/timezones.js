import {setTimezones} from "../actions/timezones";
import axios from "axios";

export function httpListTimezones(){
  return (dispatch) => {
    axios({
      method: 'GET',
      url: `${process.env.REACT_APP_BASE_URL}/api/v1/timezones`,
    }).then(response => dispatch(setTimezones(response.data)));
  }
}

export function httpSearchTimezones(search){
  return (dispatch) => {
    axios({
      method: 'POST',
      url: `${process.env.REACT_APP_BASE_URL}/api/v1/timezones/search`,
      data: {
        name: search
      }
    }).then(response => dispatch(setTimezones(response.data)));
  }
}
