import { TIMEZONES_LIST_SET } from '../const/timezones'

export function setTimezones(data){
  return {
      type: TIMEZONES_LIST_SET,
      data : data
  }
}
