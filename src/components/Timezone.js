import React from "react"
import PropTypes from "prop-types"
import moment from "moment";
import './Timezone.css'

function Timezone(props){

  return (
    <div className="Timezone">
      <div className="name">{props.name}</div>
      <div className="time"> { moment().add(props.hours, 'hours').format("HH:mm:ss") } </div>
      <div className="gmt"> GMT{props.hours < 0 ? props.hours : `+${props.hours}`} </div>
    </div>
  )
}

Timezone.propTypes = {
  name: PropTypes.string.isRequired,
  hours: PropTypes.number.isRequired,
  mins: PropTypes.number.isRequired
}

export default Timezone
