import React, {useEffect, useState} from "react"
import {connect } from "react-redux"
import {map} from "lodash"
import {httpListTimezones, httpSearchTimezones} from "../redux/http/timezones";
import './Timezones.css'
import search from "../search.png"
import Timezone from "./Timezone";

function Timezones(props) {

  const [, setTicker] = useState(Date.now())

  useEffect(()=>{
    props.httpListTimezones()
    
    setInterval(()=>{
      setTicker(Date.now())
    }, 1000)
  }, [])

  const searchOnChange = (e) => {
    props.httpSearchTimezones(e.target.value)
  }

  return (
    <div className="Timezones">
      <div className="searchBox">
        <img className="searchIcon" src={search} alt="search" width="16" height="16"/>
        <input className="searchField" type="text" placeholder="Search" name="search" onChange={(e) => searchOnChange(e)}/>
      </div>
      {
        props.timezones &&
        map(props.timezones, timezone => {
          const {id, name, hours, mins} = timezone
          return (
            <Timezone key={id} name={name} hours={hours} mins={mins} />
          )
        })
      }
    </div>
  )
}

let mapStateToProps = (state) => ({
  timezones: state.timezonesState.list,
})

let mapDispatchToProps = (dispatch) => ({
  httpListTimezones: () => dispatch(httpListTimezones()),
  httpSearchTimezones: (search) => dispatch(httpSearchTimezones(search)),
})

export default connect(mapStateToProps, mapDispatchToProps)(Timezones);
